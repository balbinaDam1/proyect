//objeciones, los comentarios que estan arriba del codigo se refieren a un grupo de lineas y cuando estan al lado se refieren a una linea en concreto
package com.balbina.proyecto;

import java.io.*;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class App
{
	private static File f = null;
	private static RandomAccessFile raf = null;
	private static final int longitudString = 100; //se utiliza para ponerle un tamaño fijo a todos los STRING que usemos.
	private static final int TAM = Double.SIZE/8+1+2*longitudString; //aquí sacamos el tamaño que tendra cada registro, el double es del precio, el 1 es del disponible(booleano) y luego el longitudString que sera del tamaño que tenga el string y lo múltiplicamos por dos (que son: nombre y compañia)
	private static int cod; //cod, se usa para LA POSICIÓN DONDE SE GUARDARÁ LA INFORMACIÓN
	private static double precio; //precio del videojuego 
	private static String nombre; //nombre, se usará para el título del videojuego
	private static String compañia; //compañia, se usará para el nombre de la compañía
	private static boolean disponible; //se utilizará para saber si ese juego esta reservado o no.
	private static JFrame jf;
	private static JPanel jp;
	private static JPanel jp2;
	private static JLabel jlCod;
	private static JTextField jtfCod;
	private static JLabel jlNom;
	private static JTextField jtfNom;
	private static JLabel jlCom;
	private static JTextField jtfCom;
	private static JLabel jlPre;
	private static JTextField jtfPre;
	private static JButton jb;
	private static JTextArea jta;
	private static JScrollPane jsp;
	private static JButton jbAlquilar;
	private static JButton jbDevolver;
	private static JButton jbBorrar;
	private static Vector<String> v;//este vector de string se usará a la hora de guardar los resultados de la consulta
	 
 	public static void main( String[] args )
    {
		jf = new JFrame("Gestor de videojuegos");
		jp = new JPanel(new GridLayout(1,0));
		jp2 = new JPanel();
		//en esta parte creamos el menu y las opciones que va a usar
		JMenuBar nav = new JMenuBar();
		JMenuItem jmAñadir = new JMenuItem("Añadir");
		JMenu jmConsultas = new JMenu("Consultas");//este va a ser un botón que cuando le cliques mostrará las opciones que estan en una tabulación más
			JMenuItem jmDisponibles = new JMenuItem("Juegos disponibles");
			JMenuItem jmNoDisponibles = new JMenuItem("Juegos no disponibles");
			JMenuItem jmTodos = new JMenuItem("Todos los juegos");
			JMenuItem jmUno = new JMenuItem("Juego en concreto");
		JMenuItem jmAlquilar = new JMenuItem("Alquilar");
		JMenuItem jmDevolver = new JMenuItem("Devolver");
		JMenuItem jmEliminar = new JMenuItem("Eliminar");
		
		JLabel jl = new JLabel("Selecciona una de las opciones que te aparecen en el menú:");
		//aqui añadimos las opciones que irán en el botón consultas
		jmConsultas.add(jmDisponibles);
		jmConsultas.add(jmNoDisponibles);
		jmConsultas.add(jmTodos);
		jmConsultas.add(jmUno);
		//aqui añadimos las opciones que irán en el menú
		nav.add(jmAñadir);
		nav.add(jmConsultas);
		nav.add(jmAlquilar);
		nav.add(jmDevolver);
		nav.add(jmEliminar);
		//añadimos el label (con el mensaje principal) en el panel y luego en la ventana le ponemos el menu creado anteriormente y le añadimos el panel
		jp.add(jl);
		jf.setJMenuBar(nav);
		jf.add(jp);
		
		jf.setLocationRelativeTo(null);//situamos el programa en el centro de la pantalla
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setSize(new Dimension(500,200));//le especificamos el tamaño que va a tener
		jf.setVisible(true);
		
		jmAñadir.addActionListener(new ActionListener() //evento para cuando le damos al botón de añadir (menu)
		{
			public void actionPerformed(ActionEvent e)
			{
				//borramos el contenido de los paneles, para que cuando se vuelva a situar en este botón se borre todo lo que contenia de antes
				jp.removeAll();
				jp2.removeAll();
				jp2 = new JPanel(new GridLayout(5,2));//creamos el segundo panel que contendrá los campos para añadir un nuevo videojuego (y este panel se pondra en el primer panel)
				//a continuación se crea las etiquetas y los textfield para pedir y guardar la información que escribe el usuario
				jlCod = new JLabel("Introduce el código:");
				jtfCod = new JTextField();
			
				jlNom = new JLabel("Introduce el nombre:");
				jtfNom = new JTextField();
			
				jlCom = new JLabel("Introduce la compañia:");
				jtfCom = new JTextField();
				
				jlPre = new JLabel("Introduce el precio:");
				jtfPre = new JTextField();
				//creamos dos botones, uno para guardar la información y otro para que los textfield anteriores se pongan vacios
				jb = new JButton("Aceptar");
				JButton jbResetear = new JButton("Resetear valores");
				//aqui añadimos todos los elementos al segundo panel
				jp2.add(jlCod);
				jp2.add(jtfCod);
				jp2.add(jlNom);
				jp2.add(jtfNom);
				jp2.add(jlCom);
				jp2.add(jtfCom);
				jp2.add(jlPre);
				jp2.add(jtfPre);
				jp2.add(jb);
				jp2.add(jbResetear);
				
				jp.add(jp2);//aqui añadimos el segundo panel al primero
				jf.pack();//aquí hacemos que la ventana se ajuste a los nuevos elementos introducidos, aviso, se le pone esto porque si lo quito cuando cambio de sección, no se me muestra lo que contiene (se queda como la imagen congelada pero que cuando se mueve la ventana manualmente, vuelve a mostrarse los elementos que contiene en esta sección)
				jf.setSize(new Dimension(500,200));//luego modificamos el tamaño para que se ponga ese en concreto
	
				jb.addActionListener(new ActionListener()//evento para cuando le da al botón de aceptar
				{
					public void actionPerformed(ActionEvent e)
					{
						String nombre = jtfNom.getText();
						if(nombre.length() > longitudString) //comprobamos que el nombre del videojuego no supere el tamaño máximo que permitimos.
							nombre = nombre.substring(0,longitudString); //si supera ese tamaño, cortamos el string hasta esa posición.
							
						String compañia = jtfCom.getText();
						if(compañia.length() > longitudString) //comprobamos que el nombre de la compañia no supere el tamaño máximo que permitimos.
							compañia = compañia.substring(0,longitudString);//si supera ese tamaño, cortamos el string hasta esa posición.

						int cod = añadir(Integer.parseInt(jtfCod.getText()),nombre,compañia,Double.parseDouble(jtfPre.getText()));//aquí guardamos la información introducida por el usuario, y nos devuelve el código donde se ha guardado esa información
						
						if(cod == Integer.parseInt(jtfCod.getText())) //aquí comprobamos si el código introducido por el usuario es el mismo que el código donde se ha guardado, si es asi, significa que el usuario habia introducido un código que estaba libre para usarse
							JOptionPane.showMessageDialog(jb,"Guardado con éxito");//con esto mostraría un mensaje a pantalla
						else//en caso de que sea distinto significa que el usuario ha introducido un código que ya estaba ocupado por otra información
						{
							String s = "Ese código ya esta cogido, se le modificará automáticamente. Su código es: " + cod;
							JOptionPane.showMessageDialog(jb,s);
						}
					}
				});//fin Listener jb
				
				jbResetear.addActionListener(new ActionListener()//evento cuando pulsa sobre el botón de resetear
				{
					public void actionPerformed(ActionEvent e)
					{
						//aqui se encarga de poner todos los textfield para que su contenido este vacio
						jtfCod.setText("");
						jtfNom.setText("");
						jtfCom.setText("");
						jtfPre.setText("");
					};
				});//fin Listener jbResetear
			}
		});//fin Listener jmAñadir
		
		jmDisponibles.addActionListener(new ActionListener()//evento para cuando se pulsa sobre el botón de disponibles (menu)
		{
			public void actionPerformed(ActionEvent e)
			{
				jp.removeAll();
				jp2.removeAll();
				jlNom = new JLabel("Videojuegos disponibles:");
				jta = new JTextArea();
				jsp = new JScrollPane(jta);//se usa el scrollpane para cuando el area del textarea sea pequeño, muestre las barras de direccionamiento y con esto ya se podria ver la información que contiene
				
				disponible = true; //con este true se lo pasamos al método mostrar(boolean) para que solo coja los videojuegos disponibles.
				v = mostrar(disponible);
				for(int i = 0; i < v.size(); i++) //este for se encarga de añadir todos los videojuegos que contiene el vector al textarea.
					jta.append(v.get(i)+"\n");
				jta.setEditable(false);//aqui ponemos ese textarea para que no se pueda modificar
				jp.add(jlNom);
				jp.add(jsp);
				jf.pack();
				jf.setSize(new Dimension(500,200));
			}
		});//fin Listener jmDisponibles
		
		jmNoDisponibles.addActionListener(new ActionListener()//evento para cuando se pulsa sobre el botón de no disponibles (menu)
		{
			public void actionPerformed(ActionEvent e)
			{
				jp.removeAll();
				jp2.removeAll();
				jlNom = new JLabel("Videojuegos no disponibles:");
				jta = new JTextArea();
				jsp = new JScrollPane(jta);
				
				disponible = false; //con este false se lo pasamos al método mostrar(boolean) para que solo coja los videojuegos NO disponibles.
				Vector<String> v = mostrar(disponible);
				for(int i = 0; i < v.size(); i++)
					jta.append(v.get(i)+"\n");
				jta.setEditable(false);
				jp.add(jlNom);
				jp.add(jsp);
				jf.pack();
				jf.setSize(new Dimension(500,200));
			}
		});//fin Listener jmNoDisponibles
		
		jmTodos.addActionListener(new ActionListener()//evento para cuando se pulsa sobre el botón de todos los juegos (menu)
		{
			public void actionPerformed(ActionEvent e)
			{
				jp.removeAll();
				jp2.removeAll();
				jlNom = new JLabel("Lista de videojuegos:");
				jta = new JTextArea();
				jsp = new JScrollPane(jta);

				v = mostrar();
				for(int i = 0; i < v.size(); i++)
					jta.append(v.get(i)+"\n");
				jta.setEditable(false);
				jp.add(jlNom);
				jp.add(jsp);
				jf.pack();
				jf.setSize(new Dimension(500,200));
			}
		});//fin Listener jmTodos
		
		jmUno.addActionListener(new ActionListener()//evento para cuando se pulsa sobre el botón de juego en concreto (menu)
		{
			public void actionPerformed(ActionEvent e)
			{
				jp.removeAll();
				jp2.removeAll();
				jp2.setLayout(new GridLayout(2,2));
				jlCod = new JLabel("Introduce el código:");
				jtfCod = new JTextField();
				jb = new JButton("Aceptar");
				jp2.add(jlCod);
				jp2.add(jtfCod);
				jp2.add(jb);
				jp.add(jp2);
				jf.pack();
				jf.setSize(new Dimension(500,200));
				
				jb.addActionListener(new ActionListener()//evento para cuando se pulsa sobre el botón aceptar
				{
					public void actionPerformed(ActionEvent e)
					{
						cod = Integer.parseInt(jtfCod.getText());
						if(contenido(cod) == 1)
						{
							for(int i=0; i < jp2.getComponentCount();i++)
							{
								Component componente = jp2.getComponent(i);
								if(componente == jsp)
								{
									jp2.remove(jlNom);
									jp2.remove(jsp);
								}
							}						

							v = mostrar(cod);
							jlNom = new JLabel("INFORMACIÓN->");
							jta = new JTextArea(v.get(0));
							jsp = new JScrollPane(jta);
							jta.setEditable(false);
							jp2.add(jlNom);
							jp2.add(jsp);
							jf.pack();
							jf.setSize(new Dimension(500,200));
						}
						else //en caso contrario mostrará que no existe ese videojuego (ya que con esto evitamos que se muestre la posición que no tenga ningún juego guardado)
							JOptionPane.showMessageDialog(jb,"No existe un videojuego con ese código.");	
					}
				});//fin Listener jb
			}
		});//fin Listener jmUno
		
		jmAlquilar.addActionListener(new ActionListener() //evento para cuando se pulsa el botón alquilar (menu)
		{
			public void actionPerformed(ActionEvent e)
			{
				jp.removeAll();
				jp2.removeAll();
				jp2.setLayout(new GridLayout(2,2));
				jlCod = new JLabel("Introduce el código:");
				jtfCod = new JTextField();
				jb = new JButton("Aceptar");
				jp2.add(jlCod);
				jp2.add(jtfCod);
				jp2.add(jb);
				jp.add(jp2);
				jf.pack();
				jf.setSize(new Dimension(500,200));
			
				jb.addActionListener(new ActionListener()//evento para cuando se le da al botón aceptar
				{
					public void actionPerformed(ActionEvent e)
					{
						for(int i=0; i < jp2.getComponentCount();i++)
						{
							Component componente = jp2.getComponent(i);
							if(componente == jbAlquilar)
							{
								jp2.remove(jlNom);
								jp2.remove(jsp);
								jp2.remove(jbAlquilar);
							}
						}
						cod = Integer.parseInt(jtfCod.getText());
						if(contenido(cod) == 1)
						{
							v = mostrar(cod);
							jlNom = new JLabel("INFORMACIÓN->");
							jta = new JTextArea(v.get(0));
							jsp = new JScrollPane(jta);
							jbAlquilar = new JButton("Alquilar");
							jta.setEditable(false);
							jbAlquilar.setEnabled(false);
							jp2.add(jlNom);
							jp2.add(jsp);
							jp2.add(jbAlquilar);
							jf.pack();
							jf.setSize(new Dimension(500,200));
								
							if(disponibilidad(cod))//si el videojuego introducido tiene disponibilidad a true, entonces si que le permitirá alquilarlo
							{
								jbAlquilar.setEnabled(true);
								jbAlquilar.addActionListener(new ActionListener() //evento para cuando se le da al botón de alquilar
								{
									public void actionPerformed(ActionEvent e)
									{
										int seleccion = JOptionPane.showConfirmDialog(jbAlquilar,"¿Estas seguro ?","Aviso", JOptionPane.YES_NO_OPTION);
										if(seleccion == 0) //significa que le ha dado que si a alquilar
										{
											operacion(cod,1);
											JOptionPane.showMessageDialog(jb,"Operación realizada con exito");
											jbAlquilar.setEnabled(false);
											v = mostrar(cod);
											jta.setText(v.get(0));//volvemos a mostrar el mismo código con la información actualizada
										}
										else
											JOptionPane.showMessageDialog(jb,"Operación realizada con exito");
									}
								});//fin Listener jbAlquilar
							}//fin IF interno
						}
						else //en caso contrario mostrará que no existe ese videojuego (ya que con esto evitamos que se muestre la posición que no tenga ningún juego guardado)
							JOptionPane.showMessageDialog(jb,"No existe un videojuego con ese código.");
					}
				});//fin Listener jb
			}
		});//fin Listener jmAlquilar
		
		jmDevolver.addActionListener(new ActionListener()//evento para cuando se le pulsa al botón de devolver (menu)
		{	
			public void actionPerformed(ActionEvent e)
			{
				jp.removeAll();
				jp2.removeAll();
				jp2.setLayout(new GridLayout(2,2));
				jlCod = new JLabel("Introduce el código:");
				jtfCod = new JTextField();
				jb = new JButton("Aceptar");
				jp2.add(jlCod);
				jp2.add(jtfCod);
				jp2.add(jb);
				jp.add(jp2);
				jf.pack();
				jf.setSize(new Dimension(500,200));
			
				jb.addActionListener(new ActionListener()//evento para cuando se le pulsa al botón de aceptar
				{
					public void actionPerformed(ActionEvent e)
					{
						for(int i=0; i < jp2.getComponentCount();i++)
						{
							Component componente = jp2.getComponent(i);
							if(componente == jbDevolver)
							{
								jp2.remove(jlNom);
								jp2.remove(jsp);
								jp2.remove(jbDevolver);
							}
						}
						cod = Integer.parseInt(jtfCod.getText());
						if(contenido(cod) == 1)
						{
							v = mostrar(cod);
							jlNom = new JLabel("INFORMACIÓN->");
							jta = new JTextArea(v.get(0));
							jsp = new JScrollPane(jta);
							jbDevolver = new JButton("Devolver");
							jta.setEditable(false);
							jbDevolver.setEnabled(false);
							jp2.add(jlNom);
							jp2.add(jsp);
							jp2.add(jbDevolver);
							jf.pack();
							jf.setSize(new Dimension(500,200));
							
							if(!disponibilidad(cod)) //con este if nos aseguramos de que solo mostrará la opción de devolver el juego en caso de que ese videojuego NO este disponible (porque eso significa que lo han alquilado y que quieren devolverlo)
							{
								jbDevolver.setEnabled(true);
								
								jbDevolver.addActionListener(new ActionListener() //evento para cuando se pulsa al botón devolver
								{
									public void actionPerformed(ActionEvent e)
									{
										int seleccion = JOptionPane.showConfirmDialog(jbAlquilar,"¿Estas seguro ?","Aviso", JOptionPane.YES_NO_OPTION);
										if(seleccion == 0) //significa que le ha dado que si a devolver
										{
											operacion(cod,2);
											JOptionPane.showMessageDialog(jb,"Operación realizada con exito");
											jbDevolver.setEnabled(false);
											v = mostrar(cod);
											jta.setText(v.get(0));
										}
										else
											JOptionPane.showMessageDialog(jb,"Operación realizada con exito");
									}
								});//fin Listener jbALquilar
							}//fin IF interno
						}
						else //en caso contrario mostrará que no existe ese videojuego (ya que con esto evitamos que se muestre la posición que no tenga ningún juego guardado)
							JOptionPane.showMessageDialog(jb,"No existe un videojuego con ese código.");
					}
				});//fin Listener jb
			}
		});//fin Listener jmDevolver
		
		jmEliminar.addActionListener(new ActionListener()//evento para cuando se le pulsa al botón de eliminar (menu)
		{
			public void actionPerformed(ActionEvent e)
			{
				jp.removeAll();
				jp2.removeAll();
				jp2.setLayout(new GridLayout(2,2));
				jlCod = new JLabel("Introduce el código:");
				jtfCod = new JTextField();
				jb = new JButton("Aceptar");
				jp2.add(jlCod);
				jp2.add(jtfCod);
				jp2.add(jb);
				jp.add(jp2);
				jf.pack();
				jf.setSize(new Dimension(500,200));
			
				jb.addActionListener(new ActionListener()//evento para cuando se le pulsa al botón de aceptar
				{
					public void actionPerformed(ActionEvent e)
					{
						for(int i=0; i < jp2.getComponentCount();i++)
						{
							Component componente = jp2.getComponent(i);
							if(componente == jbBorrar)
							{
								jp2.remove(jlNom);
								jp2.remove(jsp);
								jp2.remove(jbBorrar);
							}
						}
						cod = Integer.parseInt(jtfCod.getText());
						if(contenido(cod) == 1)
						{
							jlNom = new JLabel("INFORMACIÓN->");
							v = mostrar(cod);
							jta = new JTextArea(v.get(0));
							jsp = new JScrollPane(jta);
							jbBorrar = new JButton("Borrar");
							jta.setEditable(false);
							jp2.add(jlNom);
							jp2.add(jsp);
							jp2.add(jbBorrar);
							jf.pack();
							jf.setSize(new Dimension(500,200));
					
							jbBorrar.addActionListener(new ActionListener() //evento para cuando se le da al boton de borrar
							{
								public void actionPerformed(ActionEvent e)
								{
									int seleccion = JOptionPane.showConfirmDialog(jbAlquilar,"¿Estas seguro ?","Aviso", JOptionPane.YES_NO_OPTION);
								if(seleccion == 0) //significa que le ha dado que si a borrar
								{
									borrar(cod);
									JOptionPane.showMessageDialog(jb,"Operación realizada con exito");
									//lo eliminamos para que no muestre la información de un videojuego ya borrado
									jp2.remove(jlNom);
									jp2.remove(jsp);
									jp2.remove(jbBorrar);
									jf.pack();
									jf.setSize(new Dimension(500,200));
								}
								else
									JOptionPane.showMessageDialog(jb,"Operación realizada con exito");
								}
							});//fin Listener jbBorrar
						}
						else//en caso contrario mostrará que no existe ese videojuego (ya que con esto evitamos que se muestre la posición que no tenga ningún juego guardado)
							JOptionPane.showMessageDialog(jb,"No existe un videojuego con ese código.");
					}
				});//fin Listener jb
			}
		});//fin Listener jmEliminar
    } //fin MAIN
    
    public static int añadir(int codigo,String nom, String com,double pre) //método que se encarga de añadir el videojuego al fichero
	{
		try{
			int seguridad=100; //seguridad, se usará EN CASO DE QUE YA ESTE LA POSICIÓN OCUPADA		
			disponible = true; //especificamos que sea verdadero, porque el juego es nuevo y en un principio estaria disponible
			f = new File("videojuegos.dat");
			
			if(contenido(codigo) == 0) //en caso de que este vacio se escribirá en ese mismo sitio
			{
				raf = new RandomAccessFile(f,"rw"); //abrimos el flujo para poder escribir
				raf.seek((codigo-1)*TAM); //volvemos a la posición que queria escribir el usuario y a continuación guardamos la información.
				raf.writeUTF(nom); 
				raf.writeUTF(com);
				raf.writeDouble(pre);
				raf.writeBoolean(disponible);
			}
			else //en caso de que la posición este ocupada
			{	//PRIMER NIVEL(seguridad)
				codigo+=seguridad; //al código del usuario le sumamos 100 (que es la posición de seguridad)
				if(contenido(codigo) == 0) //si esta vacio escribirá en la misma posición
				{
					raf = new RandomAccessFile(f,"rw"); //volvemos a abrir el flujo para escribir (ya que se cierra en cuento se utiliza otro método dentro del método actual)
					raf.seek((codigo-1)*TAM); //nos situamos en esa posición de nuevo y a continuación le especificamos al usuario donde se va a guardar y guardamos la información.
//					System.out.println("Ese código ya esta cogido, se le modificará automáticamente. Su código es: " + codigo);
					raf.writeUTF(nom); 
					raf.writeUTF(com);
					raf.writeDouble(pre);
					raf.writeBoolean(disponible);
					return codigo;
				}	
				else //en caso de que esa posición este ocupada
				{	//SEGUNDO NIVEL
					codigo+=(seguridad*10); //le sumamos 1000 a la posición introducida por el usuario			
					if(contenido(codigo) == 0) //si esta vacio escribirá en la misma posición
					{
						raf = new RandomAccessFile(f,"rw");//volvemos a abrir el flujo para escribir (ya que se cierra en cuento se utiliza otro método dentro del método actual)
						raf.seek((codigo-1)*TAM); //se vuelve a situar en la posición y a continuación le especifica al usuario donde se va a guardar y guarda la información.
//						System.out.println("Ese código ya esta cogido, se le modificará automáticamente. Su código es: " + codigo);
						raf.writeUTF(nom); 
						raf.writeUTF(com);
						raf.writeDouble(pre);
						raf.writeBoolean(disponible);
						return codigo;
					}
					else //en caso de que esa posición este ocupada
					{	//TERCER NIVEL
						codigo+=(seguridad*100); //se le suma 10.000 a esa posición y le informa al usuario de donde se va a guardar y guarda esa información.
						raf = new RandomAccessFile(f,"rw");//volvemos a abrir el flujo para escribir (ya que se cierra en cuento se utiliza otro método dentro del método actual)
						raf.seek((codigo-1)*TAM); //se posiciona en esa posición.
//						System.out.println("Ese código ya esta cogido, se le modificará automáticamente. Su código es: " + codigo);
						raf.writeUTF(nom); 
						raf.writeUTF(com);
						raf.writeDouble(pre);
						raf.writeBoolean(disponible);
						return codigo;
					}//fin ELSE de la posición ocupada, TERCER NIVEL(seguridad)
				}//fin ELSE SEGUNDO NIVEL(seguridad)
			}//fin ELSE PRIMER NIVEL(seguridad)
		}//fin TRY
		catch(FileNotFoundException e)
		{
			System.err.println("El fichero donde se almacenaba la información no se encuentra disponible.");
		}
		catch(EOFException e)
		{
			//no queremos que haga nada, lo ponemos debido a que EOFException hereda del IOException, cuando este termine de leer el fichero muestra un null por pantalla (que es debido porque sale como un error entonces lo intercepta el IOException y este muestra su mensaje), entonces lo ponemos para evitar que muestre ese null
		}
		catch(IOException e)
		{
			System.err.println("ESTE ERROR ES POR MÉTODO AÑADIR: " + e.getMessage());
		}
		finally
		{
			try{
				if(raf != null) raf.close();
			}
			catch(IOException e)
			{
				System.err.println("ESTE ERROR ES POR CERRAR EL FLUJO EN EL MÉTODO AÑADIR: " + e.getMessage());
			}
		}//fin FINALLY
		return codigo;
	} //fin AÑADIR
	
	public static Vector<String> mostrar() //mostrará todos los videojuegos
	{
		v = new Vector<String>(100,100);
		try{
			f = new File("videojuegos.dat");
			raf = new RandomAccessFile(f,"rw");
			cod=1;//empezamos desde la primera posición
			raf.seek((cod-1)*TAM);//nos posicionamos en ella
			while(true) //este bucle se usa para que se repita durante todo el fichero.
			{	//ojo, en el bucle no usamos el método contiene() porque después tendriamos que volver a abrir el flujo y asi sucesivamente, entonces se crearia un bucle infinito, de ahí que lo compruebe directamente el y no a través del método contiene();
				nombre = raf.readUTF(); //leemos la información del nombre que tiene en esa posición.
				if(!nombre.isEmpty()) //comprobamos que contiene información, en caso de que tenga, lo añadirá al vector.
				{
					compañia = raf.readUTF(); //aqui leemos la información de la compañia
					precio = raf.readDouble(); //aqui leemos la información del precio
					disponible = raf.readBoolean(); //aqui leemos la información de la disponibilidad del juego
					if(disponible) //este if se utiliza para poner el texto correspondiente en la parte de disponibilidad
						v.add("\nCódigo: " + cod + "\nTítulo: " + nombre + "\nCompañia: " + compañia + "\nPrecio: " + precio + "\nDisponibilidad: Disponible"); //lo añadimos al vector
					else 
						v.add("\nCódigo: " + cod + "\nTítulo: " + nombre + "\nCompañia: " + compañia + "\nPrecio: " + precio + "\nDisponibilidad: No disponible"); //lo añadimos al vector
					cod++; //cambiamos a la siguiente posición.
					raf.seek((cod-1)*TAM); //nos posicionamos en ella
				}
				else//en caso de que NO contenga información, leerá la información que contiene pero NO lo guardará en el vector.
				{
					compañia = raf.readUTF();
					precio = raf.readDouble();
					disponible = raf.readBoolean();
					cod++; //cambiamos a la siguiente posición
					raf.seek((cod-1)*TAM);// nos posicionamos en ella	
				}//fin ELSE
			}//fin WHILE
		}//fin TRY
		catch(FileNotFoundException e)
		{
			System.err.println("El fichero donde se almacenaba la información no se encuentra disponible.");
		}
		catch(EOFException e)
		{
		}
		catch(IOException e)
		{
			System.err.println("ESTE ERROR ES POR MÉTODO MOSTRAR(): " + e.getMessage());
		}
		finally
		{
			try{
				if(raf != null) raf.close();
			}
			catch(IOException e)
			{
				System.err.println("ESTE ERROR ES POR CERRAR EL FLUJO EN EL MÉTODO MOSTRAR(): " + e.getMessage());
			}
		} //fin FINALLY
		return v;
	}//fin MOSTRAR
	
	public static Vector<String> mostrar(boolean ver) //mostrará los videojuegos dependiendo de su disponibilidad (de si quiere mostrar los disponibles o solo quiere mostrar los NO disponibles)
	{
		v = new Vector<String>(100,100);
		try{
			f = new File("videojuegos.dat");
			raf = new RandomAccessFile(f,"rw");
			cod=1; //empezamos desde la primera posición
			raf.seek((cod-1)*TAM); //nos posicionamos en esa posición
		
			while(true) //este bucle se usa para que se repita durante todo el fichero. 
			{//ojo, en el bucle no usamos el método contiene() porque después tendriamos que volver a abrir el flujo y asi sucesivamente, entonces se crearia un bucle infinito, de ahí que lo compruebe directamente el y no a través del método contiene();
				nombre = raf.readUTF(); //leemos el nombre del videojuego que tendra esa posición
				if(!nombre.isEmpty()) //si ese nombre contiene información, leerá toda la información que tendra después
				{						
					compañia = raf.readUTF(); //leemos la información de la compañia
					precio = raf.readDouble(); //leemos la información del precio
					disponible = raf.readBoolean(); //leemos la información de la disponibilidad del juego.
					if(disponible == ver) //comprobamos que ese videojuego su disponibilidad es igual a la que ha especificado el switch en main (es decir, si solo quiere mostrar los videojuegos disponibles o solo quiere mostrar los videojuegos NO disponibles)
					{
						if(disponible) //aquí comprobamos cual de los dos es para poder añadir el texto de la disponibilidad del videojuego que le corresponda
							v.add("\nCódigo: " + cod + "\nTítulo: " + nombre + "\nCompañia: " + compañia + "\nPrecio: " + precio + "\nDisponibilidad: Disponible");
						else
							v.add("\nCódigo: " + cod + "\nTítulo: " + nombre + "\nCompañia: " + compañia + "\nPrecio: " + precio + "\nDisponibilidad: No disponible");
					}//fin IF interno
					cod++; //cambiamos al siguiente código.
					raf.seek((cod-1)*TAM); //nos posicionamos en la siguiente posición
				}
				else //En caso de que ESE NOMBRE NO TENGA INFORMACIÓN EN EL, leeria su siguiente información pero NO la guardaria en el vector.
				{
					compañia = raf.readUTF();
					precio = raf.readDouble();
					disponible = raf.readBoolean();
					cod++; //cambiamos a la siguiente posición
					raf.seek((cod-1)*TAM); //nos situamos en esa posición.	
				}//fin ELSE
			}//fin WHILE
		}//fin TRY
		catch(FileNotFoundException e)
		{
			System.err.println("El fichero donde se almacenaba la información no se encuentra disponible.");
		}
		catch(EOFException e)
		{
		}
		catch(IOException e)
		{
			System.err.println("ESTE ERROR ES POR MÉTODO MOSTRAR(BOOLEAN): " + e.getMessage());
		}
		finally
		{
			try{
				if(raf != null) raf.close();
			}
			catch(IOException e)
			{
				System.err.println("ESTE ERROR ES POR CERRAR EL FLUJO EN EL MÉTODO MOSTRAR(BOOLEAN): " + e.getMessage());
			}
		} //fin FINALLY
		return v;
	}//fin MOSTRAR(boolean)
	
	public static Vector<String> mostrar(int pos) //método que se encarga de mostrar SOLO UN VIDEOJUEGO
	{
		v = new Vector<String>(1);
		try{
			f = new File("videojuegos.dat");
			raf = new RandomAccessFile(f,"rw");
		
			raf.seek((pos-1)*TAM); //nos posicionamos con el código que ha especificado el usuario y a continuación mostramos la información de ese videojuego.					
			nombre = raf.readUTF(); //leemos la información del nombre
			compañia = raf.readUTF();//leemos la información de la compañia
			precio = raf.readDouble();//leemos la información del precio
			disponible = raf.readBoolean(); //leemos la información de la disponibilidad
		
			if(disponible) //aquí comprobamos si ese juego esta disponible o no para asignarle el texto que le corresponde en la parte de disponibilidad.
				v.add("\nCódigo: " + pos + "\nTítulo: " + nombre + "\nCompañia: " + compañia + "\nPrecio: " + precio + "\nDisponibilidad: Disponible");
			else
				v.add("\nCódigo: " + pos + "\nTítulo: " + nombre + "\nCompañia: " + compañia + "\nPrecio: " + precio + "\nDisponibilidad: No disponible");
		}//fin TRY
		catch(FileNotFoundException e)
		{
			System.err.println("El fichero donde se almacenaba la información no se encuentra disponible.");
		}
		catch(EOFException e)
		{
		}
		catch(IOException e)
		{
			System.err.println("ESTE ERROR ES POR MÉTODO MOSTRAR(INT): " + e.getMessage());
		}
		finally
		{
			try{
				if(raf != null) raf.close();
			}
			catch(IOException e)
			{
				System.err.println("ESTE ERROR ES POR CERRAR EL FLUJO EN EL MÉTODO MOSTRAR(INT): " + e.getMessage());
			}
		}//fin FINALLY
		return v;
	}//fin MOSTRAR(int)
	
	public static boolean disponibilidad(int pos) //método que se encarga de devolver la disponibilidad de un videojuego.
	{
		try{
			f = new File("videojuegos.dat");
			raf = new RandomAccessFile(f,"rw");
		
			raf.seek((pos-1)*TAM); //nos posicionamos con el código que ha especificado el usuario y a continuación mostramos la información de ese videojuego.					
			nombre = raf.readUTF();//leemos la información del nombre
			compañia = raf.readUTF();//leemos la información de la compañia
			precio = raf.readDouble();//leemos la información del precio
			disponible = raf.readBoolean();//leemos la información de la disponibilidad
		}//fin TRY
		catch(FileNotFoundException e)
		{
			System.err.println("El fichero donde se almacenaba la información no se encuentra disponible.");
		}
		catch(IOException e)
		{
			System.err.println("ESTE ERROR ES POR MÉTODO DISPONIBILIDAD(): " + e.getMessage());
		}
		finally
		{
			try{
				if(raf != null) raf.close(); //aqui cerramos el flujo del raf.
			}
			catch(IOException e)
			{
				System.err.println("ESTE ERROR ES POR CERRAR EL FLUJO EN EL MÉTODO DISPONIBILIDAD(): " + e.getMessage());
			}
		}//fin FINALLY
		return disponible; //devolvemos su disponibilidad
	}//fin DISPONIBILIDAD

	public static void operacion(int cod,int opcion)// se utiliza para alquilar (1) o devolver(2) un videojuego
	{
		try{
			f = new File("videojuegos.dat");
			raf = new RandomAccessFile(f,"rw");
		
			if(opcion == 1) //en caso de que ponga un 1 significa que lo van a alquilar
			{
				raf.seek((cod-1)*TAM); //nos posicionamos en la posicion de ese videojuego
				nombre = raf.readUTF(); //leemos su nombre
				compañia = raf.readUTF(); //leemos su compañia
				precio = raf.readDouble(); //leemos su precio
				disponible = false; //cambiamos la disponibilidad y ponemos false para especificar que no esta disponible
				raf.writeBoolean(disponible); //lo escribimos en su posición
			}
			else //en caso contrario significa que lo van a devolver
			{
				raf.seek((cod-1)*TAM); //nos posicionamos en esa posición
				nombre = raf.readUTF(); //leemos su nombre
				compañia = raf.readUTF();//leemos su compañia
				precio = raf.readDouble(); //leemos su precio
				disponible = true; //cambiamos la disponibilidad a true (porque eso significa que ya lo han devuelto y que se puede volver a alquilar)
				raf.writeBoolean(disponible); //escribimos ese cambio en la posición que le corresponde en el registro.
			}
		}//fin try
		catch(FileNotFoundException e)
		{
			System.err.println("El fichero donde se almacenaba la información no se encuentra disponible.");
		}
		catch(EOFException e)
		{
		}
		catch(IOException e)
		{
			System.err.println("ESTE ERROR ES POR MÉTODO OPERACION(): " + e.getMessage());
		}
		finally
		{
			try{
				if(raf != null) raf.close();
			}
			catch(IOException e)
			{
				System.err.println("ESTE ERROR ES POR CERRAR EL FLUJO EN EL MÉTODO OPERACION(): " + e.getMessage());
			}
		} //fin FINALLY	
	}//fin OPERACION
	
	public static void borrar(int codB)
	{
		f = new File("videojuegos.dat");
		File f2 = new File(f + "new");//creamos este segundo file que es donde se guardará la información sin la información del videojuego que se quiere borrar
		RandomAccessFile raf2 = null;
		cod=1;//vamos a la primera posición
		try{
			raf = new RandomAccessFile(f,"rw");
			raf2 = new RandomAccessFile(f2,"rw");
		
			raf.seek((cod-1)*TAM);//nos posicionamos en ella
			while(true) //este bucle se utiliza para que se repita hasta que termine de leer todo el fichero
			{//ojo, en el bucle no usamos el método contiene() porque después tendriamos que volver a abrir el flujo y asi sucesivamente, entonces se crearia un bucle infinito, de ahí que lo compruebe directamente el y no a través del método contiene();
				nombre = raf.readUTF(); //leemos la información del nombre que contiene esa posición
				if(!nombre.isEmpty()) //en caso de que contenga información (es decir, no esta vacia), leerá su siguiente información
				{
					compañia = raf.readUTF();
					precio = raf.readDouble();
					disponible = raf.readBoolean();
					if (cod != codB) //con este if se utiliza para que se vaya escribiendo en el otro fichero, se escribirá todos los códigos a excepción del codigo que se quiere borrar.
					{
						raf2.seek((cod-1)*TAM); //en el segundo fichero nos posicionamos en la posición que le pertenece a ese videojuego y escribimos su información a continuación.
						raf2.writeUTF(nombre);
						raf2.writeUTF(compañia);
						raf2.writeDouble(precio);
						raf2.writeBoolean(disponible);				
					}//fin IF
					
					cod++;//cambiamos a la siguiente posición
					raf.seek((cod-1)*TAM);//nos posicionamos en ella.
				}
				else//en caso de que NO contenga información, cambiamos a la siguiente posición.
				{
					cod++; //cambiamos a la siguiente posición
					raf.seek((cod-1)*TAM);//nos posicionamos en ella
				}//fin ELSE
			}//fin WHILE interno
		}//fin TRY
		catch(EOFException e)
		{
			System.out.println(); //estético
		}
		catch(IOException e)
		{
			System.err.println("ESTE ERROR ES POR MÉTODO BORRAR(): " + e.getMessage());
		}
		finally
		{
			f.delete(); //borramos el fichero que contenia el videojuego que se queria borrar
			f2.renameTo(f); //renombramos el nuevo fichero con el nombre que contenia el anterior.
			try{
				if(raf2 != null) raf2.close(); //cerramos primero el raf2 debido a que es el ÚLTIMO en crearse antes y para no cerrar algo que raf2 necesite, se cierra primero este.
				if(raf != null) raf.close();
			}
			catch(IOException e)
			{
				System.err.println("ESTE ERROR ES POR CERRAR EL FLUJO EN EL MÉTODO BORRAR(): " + e.getMessage());
			}
		}//fin FINALLY
	}//fin BORRAR
	
	public static int contenido(int cod) //este método se encarga de comprobar que esa posición contiene alguna información, se utiliza en casos en los que necesito comprobar que contiene una información Y NO TENGA QUE USAR un bucle.
	{
		try{
			f = new File("videojuegos.dat");
			raf = new RandomAccessFile(f,"rw");
	
			raf.seek((cod-1)*TAM); //nos posicionamos en la posición del código
			nombre = raf.readUTF(); //leemos la información del nombre
		
			if(!nombre.isEmpty()) //si contiene alguna información devuelve un 1
				return 1; //el otro return esta después del finally	
		}
		catch(EOFException e)
		{
		}
		catch(IOException e)
		{
			System.err.println("ESTE ERROR ES POR MÉTODO CONTENIDO(): " + e.getMessage());
		}
		finally
		{
			try{
				if(raf != null) raf.close();
			}
			catch(IOException e)
			{
				System.err.println("ESTE ERROR ES POR CERRAR EL FLUJO EN EL MÉTODO CONTENIDO(): " + e.getMessage());
			}
		}//fin FINALLY
	//OJO, me ha tocado poner aquí el return 0, debido a que si lo dejo después del if, java no me deja, ya que podria producirse un error y no hacer el try sino que iria directamente al catch y este no devolvería nada.
		return 0;//en caso de NO tener ninguna información devuelve un 0.
	}//fin CONTENIDO
}//fin App
